import setuptools

setuptools.setup(
    name='flaskr',
    version='1.0.0',
    author='spv7016',
    author_email='spv7016@gmail.com',
    description='test package',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language  :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
python_requires='>=3.11'
)