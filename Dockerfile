# Заменим образ на более легковесный
FROM python:3.11.1-slim-buster

# Создадим нерутового (не-администратор) пользователя
RUN useradd -m python

# Создадим директорию для нашего приложения и сменим ее пользователя
RUN mkdir /home/python/app/ && chown -R python:python /home/python/app

# Продолжим работу в этой директории ("аналог cd")
WORKDIR /home/python/app

# Сменим пользователя на нового
USER python

# Укажем переменную окружения, в которой python будет искать пакеты
ENV PATH="/home/python/.local/bin:${PATH}"

# Добавим в образ наше приложение
ADD app /home/python/app

# Установим зависимости
RUN pip install -e .

# Удалим пункты по тестированию приложения внутри образа, мы это сделали в отдельном пункте пайплайна

# Инициализируем базу данных
RUN flask --app flaskr init-db

# Указываем как нужно запускать контейнер
CMD ["flask", "--app", "flaskr", "--debug", "run", "--host=0.0.0.0"]


